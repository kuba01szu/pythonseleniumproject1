import random
import string
import time
import pytest

from faker import Faker
from selenium.webdriver import Chrome
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager

fake = Faker()

email = fake.ascii_safe_email()
name = random.choice(['Ala', 'John', 'Darek', 'Michael', 'Kate'])
surname = random.choice(['Jones', 'Britton', 'Leach', 'Barrows', 'Watson'])
expire = fake.date_between(start_date='now', end_date='+10y')
expire_date = expire.strftime('%m/%Y')
digits = [str(random.randint(1000, 9999)) for _ in range(4)]
card_number = '-'.join(digits)


def get_random_cvv(length):
    return ''.join(random.choices(string.digits, k=length))


cvv = get_random_cvv(3)
card_holder_name = ''.join(name + ' ' + surname)


def get_random_string(length):
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=length))


password = get_random_string(10)


@pytest.fixture
def browser():
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.set_window_size(1366, 768)

    browser.get('https://practicesoftwaretesting.com/#/')
    yield browser
    browser.quit()


def test_register_new_account_and_log_in(browser):
    browser.find_element(By.LINK_TEXT, 'Sign in').click()
    time.sleep(3)
    browser.find_element(By.CSS_SELECTOR, 'p>a').click()

    time.sleep(3)

    browser.find_element(By.CSS_SELECTOR, '#first_name').send_keys(name)
    browser.find_element(By.CSS_SELECTOR, '#last_name').send_keys(surname)
    browser.find_element(By.CSS_SELECTOR, '#dob').send_keys('12021983')
    browser.find_element(By.CSS_SELECTOR, '#address').send_keys('Kraków')

    browser.find_element(By.CSS_SELECTOR, '#postcode').send_keys('31-300')
    browser.find_element(By.CSS_SELECTOR, '#city').send_keys('krakow')
    browser.find_element(By.CSS_SELECTOR, '#state').send_keys('Małopolska')
    browser.find_element(By.CSS_SELECTOR, '#country').click()

    time.sleep(3)

    browser.find_element(By.CSS_SELECTOR, "[value='PL']").click()
    browser.find_element(By.CSS_SELECTOR, '#phone').send_keys('42949672976')
    browser.find_element(By.CSS_SELECTOR, '#email').send_keys(email)

    time.sleep(5)

    browser.find_element(By.CSS_SELECTOR, '#password').send_keys(password)
    browser.find_element(By.CSS_SELECTOR, 'button.btnSubmit.mb-3').click()

    time.sleep(4)

    browser.find_element(By.CSS_SELECTOR, '#email').send_keys(email)
    browser.find_element(By.CSS_SELECTOR, '#password').send_keys(password)

    time.sleep(5)

    browser.find_element(By.CSS_SELECTOR, 'input.btnSubmit').click()


def test_login_registered_user(browser):
    browser.find_element(By.LINK_TEXT, 'Sign in').click()
    time.sleep(3)
    browser.find_element(By.CSS_SELECTOR, '#email').send_keys(email)
    browser.find_element(By.CSS_SELECTOR, '#password').send_keys(password)
    browser.find_element(By.CSS_SELECTOR, 'input.btnSubmit').click()


def test_search_for_product_and_add_product_to_the_basket(browser):
    wait = WebDriverWait(browser, 15)
    search_area = (By.CSS_SELECTOR, 'a.nav-link.active')
    wait.until(expected_conditions.element_to_be_clickable(search_area))
    time.sleep(20)
    products = browser.find_elements(By.CSS_SELECTOR, 'div.card-body')
    time.sleep(25)

    assert 'Combination Pliers' in products[0].text
    # assert 'Cutters' in products[2].text
    assert len(products) == 9
    time.sleep(20)

    wait.until(expected_conditions.element_to_be_clickable(search_area))
    browser.find_element(By.CSS_SELECTOR, '.form-control').send_keys('Combination pliers')
    browser.find_element(By.CSS_SELECTOR, 'button.btn.btn-secondary').click()
    time.sleep(25)
    found_product = browser.find_elements(By.CSS_SELECTOR, 'div.card-body')
    # assert 'Combination Pliers' in found_product[0].text
    assert len(found_product) == 1

    browser.find_element(By.CSS_SELECTOR, '.card-img-top').click()
    time.sleep(25)
    assert browser.find_element(By.CSS_SELECTOR, '#btn-add-to-cart').is_displayed()

    time.sleep(25)
    browser.find_element(By.CSS_SELECTOR, '#btn-add-to-cart').click()
    time.sleep(25)
    browser.find_element(By.CSS_SELECTOR, '#lblCartCount').click()
    time.sleep(25)
    product_in_basket = browser.find_elements(By.CSS_SELECTOR, 'span.product-title')
    assert 'Combination Pliers' in product_in_basket[0].text


def test_buy_product_by_logged_user(browser):
    wait = WebDriverWait(browser, 15)
    browser.find_element(By.LINK_TEXT, 'Sign in').click()

    time.sleep(3)
    browser.find_element(By.CSS_SELECTOR, 'p>a')

    browser.find_element(By.CSS_SELECTOR, 'p>a').click()
    time.sleep(3)

    browser.find_element(By.CSS_SELECTOR, '#first_name').send_keys(name)
    browser.find_element(By.CSS_SELECTOR, '#last_name').send_keys(surname)
    browser.find_element(By.CSS_SELECTOR, '#dob').send_keys('12021983')
    browser.find_element(By.CSS_SELECTOR, '#address').send_keys('Kraków')

    browser.find_element(By.CSS_SELECTOR, '#postcode').send_keys('31-300')
    browser.find_element(By.CSS_SELECTOR, '#city').send_keys('krakow')
    browser.find_element(By.CSS_SELECTOR, '#state').send_keys('Małopolska')
    browser.find_element(By.CSS_SELECTOR, '#country').click()

    last_on_the_list = (By.CSS_SELECTOR, "[value='ZW']")
    wait.until(expected_conditions.element_to_be_clickable(last_on_the_list))

    browser.find_element(By.CSS_SELECTOR, "[value='PL']").click()
    browser.find_element(By.CSS_SELECTOR, '#phone').send_keys('42949672976')
    registration_email = browser.find_element(By.CSS_SELECTOR, '#email').send_keys(email)

    submit = (By.CSS_SELECTOR, 'button.btnSubmit.mb-3')
    wait.until(expected_conditions.element_to_be_clickable(submit))
    browser.find_element(By.CSS_SELECTOR, '#password').send_keys(password)
    browser.find_element(By.CSS_SELECTOR, 'button.btnSubmit.mb-3').click()

    submit_login = (By.CSS_SELECTOR, 'input.btnSubmit')
    wait.until(expected_conditions.element_to_be_clickable(submit_login))
    login_email = browser.find_element(By.CSS_SELECTOR, '#email').send_keys(email)
    browser.find_element(By.CSS_SELECTOR, '#password').send_keys(password)
    assert registration_email == login_email

    browser.find_element(By.CSS_SELECTOR, 'input.btnSubmit').click()

    message_button = (By.CSS_SELECTOR, "[data-test='nav-messages']")
    wait.until(expected_conditions.element_to_be_clickable(message_button))
    my_account = browser.find_element(By.CSS_SELECTOR, "[data-test='page-title']")
    assert 'My account' in my_account.text

    browser.find_element(By.CSS_SELECTOR, 'a.nav-link.active').click()
    time.sleep(5)
    last_product = (By.CSS_SELECTOR, "[alt='Combination Pliers']")
    wait.until(expected_conditions.element_to_be_clickable(last_product))
    browser.find_element(By.CSS_SELECTOR, "[alt='Combination Pliers']").click()
    time.sleep(5)
    add_to_cart_button = (By.CSS_SELECTOR, '#btn-add-to-cart')
    wait.until(expected_conditions.element_to_be_clickable(add_to_cart_button))

    browser.find_element(By.CSS_SELECTOR, '#btn-add-to-cart').click()
    time.sleep(5)
    quantity_of_product = browser.find_element(By.CSS_SELECTOR, '#lblCartCount')
    assert quantity_of_product.is_displayed()

    browser.find_element(By.CSS_SELECTOR, 'i.fa.fa-shopping-cart.px-1').click()

    proceed_button = (By.CSS_SELECTOR, "[data-test='proceed-1']")
    wait.until(expected_conditions.element_to_be_clickable(proceed_button))

    browser.find_element(By.CSS_SELECTOR, "[data-test='proceed-1']").click()
    browser.find_element(By.CSS_SELECTOR, "[data-test='proceed-2']").click()
    browser.find_element(By.CSS_SELECTOR, "[data-test='proceed-3']").click()
    browser.find_element(By.CSS_SELECTOR, '#payment-method').click()
    browser.find_element(By.CSS_SELECTOR, "[value='3: Credit Card']").click()

    browser.find_element(By.CSS_SELECTOR, '#payment-method').click()

    browser.find_element(By.CSS_SELECTOR, "[data-test='credit_card_number']").send_keys(card_number)
    browser.find_element(By.CSS_SELECTOR, "[data-test='expiration_date']").send_keys(expire_date)
    browser.find_element(By.CSS_SELECTOR, "[data-test='cvv']").send_keys(cvv)
    browser.find_element(By.CSS_SELECTOR, "[data-test='card_holder_name']").send_keys(card_holder_name)
    browser.find_element(By.CSS_SELECTOR, "[data-test='finish']").click()

    confirm = (By.CSS_SELECTOR, "[data-test='finish']")
    wait.until(expected_conditions.element_to_be_clickable(confirm))

    browser.find_element(By.CSS_SELECTOR, "[data-test='finish']").click()

    time.sleep(10)
    browser.find_element(By.CSS_SELECTOR, "[data-test='finish']").click()
