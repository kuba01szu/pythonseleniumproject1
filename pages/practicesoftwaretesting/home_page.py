import time

from selenium.webdriver.common.by import By


class HomePage:
    def __init__(self, browser):
        self.browser = browser

    def visit_home_page(self):
        self.browser.get('https://practicesoftwaretesting.com/#/')

    def sign_in(self):
        self.browser.find_element(By.LINK_TEXT, 'Sign in').click()

    def check_for_product(self):
        products = self.browser.find_element(By.CSS_SELECTOR, 'h5.card-title')
        time.sleep(15)
        assert 'Combination Pliers' in products.text

    def search_for_product(self):
        self.browser.find_element(By.CSS_SELECTOR, '.form-control').send_keys('Combination pliers')
        self.browser.find_element(By.CSS_SELECTOR, 'button.btn.btn-secondary').click()
        found_product = self.browser.find_element(By.CSS_SELECTOR, 'h5.card-title')
        assert 'Combination Pliers' in found_product.text

