import time

from selenium.webdriver.common.by import By


class ProductPage:
    def __init__(self, browser):
        self.browser = browser

    def product_check(self):
        self.browser.find_element(By.CSS_SELECTOR, '.card-img-top').click()
        time.sleep(15)
        assert self.browser.find_element(By.CSS_SELECTOR, '#btn-add-to-cart').is_displayed()


