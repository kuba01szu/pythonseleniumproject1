from selenium.webdriver.common.by import By

from tests.test_practicesoftwaretesting import email, password


class LoginPage:
    def __init__(self, browser):
        self.browser = browser

    def open_registration_form(self):
        self.browser.find_element(By.CSS_SELECTOR, 'p>a').click()

    def email(self):
        self.browser.find_element(By.CSS_SELECTOR, '#email').send_keys(email)

    def password(self):
        self.browser.find_element(By.CSS_SELECTOR, '#password').send_keys(password)

    def submit(self):
        self.browser.find_element(By.CSS_SELECTOR, 'input.btnSubmit').click()

    def login_email(self):
        self.browser.find_element(By.CSS_SELECTOR, '#email').send_keys(email)

    def login_password(self):
        self.browser.find_element(By.CSS_SELECTOR, '#password').send_keys(password)