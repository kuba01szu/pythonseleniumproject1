from selenium.webdriver.common.by import By

from tests.test_practicesoftwaretesting import name, surname, email, password


class RegistrationPage:
    def __init__(self, browser):
        self.browser = browser

    def name(self):
        self.browser.find_element(By.CSS_SELECTOR, '#first_name').send_keys(name)

    def surname(self):
        self.browser.find_element(By.CSS_SELECTOR, '#last_name').send_keys(surname)

    def date_of_birth(self):
        self.browser.find_element(By.CSS_SELECTOR, '#dob').send_keys('12021983')

    def address(self):
        self.browser.find_element(By.CSS_SELECTOR, '#address').send_keys('Kraków')

    def postcode(self):
        self.browser.find_element(By.CSS_SELECTOR, '#postcode').send_keys('31-300')

    def city(self):
        self.browser.find_element(By.CSS_SELECTOR, '#city').send_keys('krakow')

    def state(self):
        self.browser.find_element(By.CSS_SELECTOR, '#state').send_keys('Małopolska')

    def country(self):
        self. browser.find_element(By.CSS_SELECTOR, '#country').click()

    def choose_country(self):
        self.browser.find_element(By.CSS_SELECTOR, "[value='PL']").click()

    def phone(self):
        self.browser.find_element(By.CSS_SELECTOR, '#phone').send_keys('42949672976')

    def email(self):
        self.browser.find_element(By.CSS_SELECTOR, '#email').send_keys(email)

    def password(self):
        self.browser.find_element(By.CSS_SELECTOR, '#password').send_keys(password)

    def submit_button(self):
        self.browser.find_element(By.CSS_SELECTOR, 'button.btnSubmit.mb-3').click()